using System;

namespace Aggregation
{
    
    public class BaseDeposit : Deposit
    {
        public BaseDeposit(decimal amount, int period) : base(amount, period)
        {
        }

       
        public override decimal Income()
        {
            decimal income = 0;
            decimal amountAdd = Amount;
            decimal incomeMonth = 0;
            for (int i = 1; i <= Period; i++)
            {
                
                incomeMonth = amountAdd * 0.05m;
                income = income + incomeMonth;
                
                amountAdd = amountAdd + incomeMonth;
                
            }

            return income;
           
        }
    }

}