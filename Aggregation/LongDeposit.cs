using System;

namespace Aggregation
{
    
    public class LongDeposit : Deposit
    {
        public LongDeposit(decimal amount, int period) : base(amount, period)
        {
        }
        
        public override decimal Income()
        {
            var monthWhenIncomeStarts = 6;
            if (Period > monthWhenIncomeStarts)
            {
                double monthIncomePercent = 0.15;
            
                var income = Amount * (decimal) Math.Pow(1 + monthIncomePercent, Period - monthWhenIncomeStarts) - Amount;

                return Decimal.Round(income, 2);
            }

            return 0;
        }
    }

}