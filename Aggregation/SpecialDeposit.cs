using System;

namespace Aggregation
{
     
    public class SpecialDeposit : Deposit
    {
        public SpecialDeposit(decimal amount, int period) : base(amount, period)
        {
        }

      
        public override decimal Income()
        {
            decimal income = Amount;

            for (int i = 1; i<=Period; i++)
            {
                income = income + income * i * 0.01m;   
                
            }
            income = Decimal.Round(income - Amount, 2);
            
            
            return income;                                     //in format 1000.00
        }
    }

}