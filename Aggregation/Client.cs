using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



namespace Aggregation
{
    
    public class Client
    {
        private Deposit[] deposits;

        public Client()
        {
            deposits = new Deposit[10];
        }

        public bool AddDeposit(Deposit deposit)
        {
            //testing TimeOuts if Student begins while(true)
            
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] == null)
                {
                    deposits[i] = deposit;

                    return true;
                }
            }
            return false;
        }

        public decimal TotalIncome()
        {
            decimal totalIncome = 0;
            
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] != null)
                {
                    totalIncome += deposits[i].Income();
                }
            }

            return Decimal.Truncate(totalIncome);             ///
        }

       
        
        public decimal MaxIncome()
        {
            Console.WriteLine("MaxIncome");
            decimal max = 0.00m;
            for(int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] != null)
                {
                    if (deposits[i].Income() >= max)
                    {
                        max = deposits[i].Income();
                    }
                }
            }
            Console.WriteLine(max);
            return max;
        }

        public decimal GetIncomeByNumber(int number)
        {
            var index = --number;
            
            if (deposits[index] != null)
            {
                return deposits[index].Income();
            }

            return 0;
        }
    }

}